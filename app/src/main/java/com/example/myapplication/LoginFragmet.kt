package com.example.myapplication

import android.content.ContentValues.TAG
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.example.myapplication.databinding.LoginFragmetBinding
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await


class LoginFragmet : Fragment() {

    private var _binding: LoginFragmetBinding? = null
    private val binding get() = _binding!!
    private lateinit var auth: FirebaseAuth


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = LoginFragmetBinding.inflate(inflater, container, false)
        auth = FirebaseAuth.getInstance()
        val root: View = binding.root
        init()

        return root
    }

    private fun init() = with(binding) {
        buttonReg.setOnClickListener {
            findNavController().navigate(R.id.moveToRegistr)
        }

        buttonLog.setOnClickListener {
            when (true) {
                editTextEmailAddress.text.toString() == "" -> {
                    Toast
                        .makeText(
                            activity,
                            getString(R.string.enter_email),
                            Toast.LENGTH_SHORT
                        ).show()
                }
                editTextPassword.text.toString() == "" -> {
                    Toast
                        .makeText(
                            activity,
                            getString(R.string.enter_password),
                            Toast.LENGTH_SHORT
                        ).show()
                }
                else -> {
                    CoroutineScope(Dispatchers.Main).launch {
                        val data = loginAccount(
                            auth,
                            editTextEmailAddress.text.toString(),
                            editTextPassword.text.toString()
                        )
                        if (data != null) {
                            findNavController().navigate(R.id.loginFragmet_to_homeFragment)

                        } else {
                            Toast.makeText(activity, "Ошибка аутентификации",
                                Toast.LENGTH_SHORT).show()
                            editTextPassword.setText("")
                        }
                    }
                }
            }
        }

    }

    private suspend fun loginAccount(firebaseAuth: FirebaseAuth,
                                     email:String,password:String): AuthResult? {
        return try {
            val data = firebaseAuth
                .signInWithEmailAndPassword(email,password)
                .await()
            Log.d(TAG, "createUserWithEmail:success")
            data
        } catch (e : Exception) {
            Log.w(TAG, "createUserWithEmail:failure", e)
            null
        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}