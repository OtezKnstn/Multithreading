package com.example.myapplication

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class WebViewModel: ViewModel() {
    val savedURL: MutableLiveData<String> by lazy {
        MutableLiveData<String>("https://www.google.ru/")
    }
}