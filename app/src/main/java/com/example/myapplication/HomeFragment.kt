package com.example.myapplication

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import com.example.myapplication.databinding.HomeFragmentBinding



class HomeFragment : Fragment() {

    lateinit var binding: HomeFragmentBinding
    private val adapter = DishAdapter()

    private val imageIdList = listOf(
        R.drawable.batinsup,
        R.drawable.hlib,
        R.drawable.ramen,
        R.drawable.voda,
        R.drawable.zmeyka
    )
    private var index = 0


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = HomeFragmentBinding.inflate(inflater, container, false)
        val view = binding.root
        init()
        return view
    }

    private fun init()= with(binding){
        rcView.layoutManager = GridLayoutManager(context, 3)
        rcView.adapter = adapter
        buttonAdd.setOnClickListener {
            if(index > 4){
                index = 0

            }
            val dish = Dishes(imageIdList[index],"Блюдо №${index+1}")
            adapter.addDish(dish)
            index++

        }
    }



    companion object {

        @JvmStatic
        fun newInstance() = HomeFragment()
    }
}